﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SaveUsersConsole
{
    class Program
    {

        public static readonly Logger _logger = LogManager.GetCurrentClassLogger(); 

        static void Main(string[] args)
        {

            if (!File.Exists("users.txt"))
            {
                CreateJsonFile();
            }

            var tokenSource2 = new CancellationTokenSource();
            CancellationToken ct = tokenSource2.Token;
            Task[] tasks = new Task[2];
            try
            {                
                tasks[0] = Task.Run(() => SaveUsersInDB(), tokenSource2.Token);
                tasks[1] = Task.Run(() => {

                    ct.ThrowIfCancellationRequested();
                    var key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Escape)
                    {
                        if (ct.IsCancellationRequested)
                        {
                            ct.ThrowIfCancellationRequested();
                        }
                        tokenSource2.Dispose();
                        Environment.Exit(0);
                    }
                }, tokenSource2.Token);                              
            }
            catch (OperationCanceledException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                tokenSource2.Dispose();
            }
            

            Task.WaitAll(tasks);
            Console.ReadLine();
        }

        public static void SaveUsersInDB()
        {
            var threadscountFromConfig = ConfigurationManager.AppSettings["ThreadsCount"];
            int threadsCount = (int.TryParse(threadscountFromConfig, out threadsCount) && threadsCount > 1) ? threadsCount : 1;
            UserRepository repository = new UserRepository();
            Parallel.ForEach(File.ReadLines("users.txt"), new ParallelOptions { MaxDegreeOfParallelism = threadsCount }, (line, _, lineNumber) =>
            {
                var user = JsonConvert.DeserializeObject<User>(line);                
                repository.SaveUser(user);
                Console.WriteLine("Пользователь с ID " + user.Id + " успешно добавлен в таблицу");
                _logger.Info("Пользователь с ID " + user.Id + " успешно добавлен в таблицу");
            });
            Console.WriteLine("Пользователи успешно добавлены в бд");
            _logger.Info("Пользователи успешно добавлены в бд");
        }

        public static void CreateJsonFile()
        {
            _logger.Info("Начал создаватся файл");
            Console.WriteLine("Начал создаватся файл");
            using (StreamWriter sw = File.CreateText("users.txt"))
            {
                for (int i = 0; i < 4000; i++)
                {
                    var user = new User
                    {
                        Id = Guid.NewGuid(),
                        FullName = i + "ФИО",
                        ChangeDate = DateTime.Now,
                        City = i + "city",
                        Email = i + "email",
                        Phone = i + "phone"
                    };
                    sw.WriteLine(JsonConvert.SerializeObject(user)); ;
                    _logger.Info("Пользователь с ID " + user.Id + " успешно добвлен в файл");
                    Console.WriteLine("Пользователь с ID " + user.Id + " успешно добвлен в файл");
                }
            }
            Console.WriteLine("Файл успешно создан");
            _logger.Info("Файл успешно создан");
        }
    }
}
