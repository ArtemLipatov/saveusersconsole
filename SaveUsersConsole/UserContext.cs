﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveUsersConsole
{
    class UserContext : DbContext
    {
        public UserContext() : base("DbConnection")
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<UserContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
        }

        public DbSet<User> Users { get; set; }
    }
}
