﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveUsersConsole
{
    class UserRepository
    {
        public void SaveUser(User user)
        {
            using (var context = new UserContext())
            {
                var userInDb = context.Users.FirstOrDefault(x => x.Id == user.Id);
                if (userInDb != null )
                {
                    if (userInDb.ChangeDate < user.ChangeDate)
                    {
                        userInDb.ChangeDate = user.ChangeDate;
                        userInDb.FullName = user.FullName;
                        userInDb.City = user.City;
                        userInDb.Email = user.Email;
                        userInDb.Phone = user.Phone;
                    }                 
                }
                else
                {
                    context.Users.Add(user);
                }
                context.SaveChanges();
            }
        }
    }
}
