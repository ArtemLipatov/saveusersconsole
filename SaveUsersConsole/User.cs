﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SaveUsersConsole
{
    [Table("User")]
    class User
    {
        [Key]
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime ChangeDate { get; set; }

        public override string ToString()
        {
            return "{\"Id\":\"" + Id.ToString() + "\", \"FullName\":\"" + FullName + "\", \"City\" : \"" + City + "\", \"Email\":\"" 
                + Email + "\", \"Phone\" : \""+ Phone + "\", \"ChangeDate\" : \"" + ChangeDate.ToString() + "\"   }";
        }
    }
}
